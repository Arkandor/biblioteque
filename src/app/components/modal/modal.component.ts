import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})

export class ModalComponent implements OnInit {
	@Input() show;
    @Input() type;
    @Input() data;


    @Output() public modalOutput = new EventEmitter();

    constructor() { 

    }

    ngOnInit() {
        
    }

    closeModal(type) {
        this.modalOutput.emit(type);
    }
}

