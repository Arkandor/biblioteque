import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'b-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

    loading:Boolean = false;

    constructor(private router: Router) { 
 
    }

    ngAfterViewInit() {

    }

    ngOnInit() {

    }
}

