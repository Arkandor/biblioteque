import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Constants } from "../../services/Constants";
import { HttpService } from "../../services/HttpService";

@Component({
    selector: 'home',
    templateUrl: './home.html',
    styleUrls: ['./home.scss']
})

export class HomeView implements OnInit {

    books:any = [];
    selectedBook:any = null;
    loading:Boolean = true;

    constructor(private httpService: HttpService, private router: Router, private constants: Constants) { 
        
    }

    ngOnInit() {

        this.getBooksList();
    }
    
    getBooksList() {
        this.constants.getBooksList().then(result => {
            this.books = result;
            this.loading = false;
        });
    }

    closeModal(e?) {
        this.selectedBook = null;
    }
}

