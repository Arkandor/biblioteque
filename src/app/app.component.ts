import { Component, ViewChild, OnInit } from '@angular/core';
import { Router, Event as RouterEvent, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

    innerHeight = window.innerHeight;
    loading = true;

    constructor(public router: Router) {
		this.router.events.subscribe((e : RouterEvent) => {
			this.navigationInterceptor(e);
		});
    }

	ngOnInit() {

	}

	navigationInterceptor(event: RouterEvent): void {
		if (event instanceof NavigationStart) {
			this.loading = true;
		}
		if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
			this.loading = false;
		}
	}
}
