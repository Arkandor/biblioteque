import { Injectable } from '@angular/core';

@Injectable()
export class Constants {

    private booksList:any = [ // hardcoded list
        {
            name: "Manuscrisul gasit la Accra",
            id: 1,
            author: "Paulo Coelho",
            release_date: "1920-04-05"
        },
        {
            name: "Enigma Otiliei",
            id: 1,
            author: "George Calinescu",
            release_date: "1920-04-05"
        },
        {
            name: "Ion",
            id: 1,
            author: "Liviu Rebreanu",
            release_date: "1920-04-05"
        }
    ];

    public getBooksList():any {
    	return new Promise((resolve, reject) => {
            setTimeout(() => { // TO BE REMOVED --- HARDCODED TO SIMULATE API RESPONSE CALL
                resolve(this.booksList);
            }, 1000);
        });
    }
}