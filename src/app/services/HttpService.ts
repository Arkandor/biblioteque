import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from './Constants';
import { RestConstants } from './RestConstants';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, tap } from 'rxjs/operators';
import 'rxjs/operators';

@Injectable()
export class HttpService {
    constructor (private http:HttpClient, private constants: Constants, private restConstants: RestConstants) {

    }

    postData(url, data) {
        return this.http.post(url, data)
            .pipe(
                tap(evt => {
                    
                 }),
                catchError(error => {
                    return throwError(error); // add this line
                 })
            );
    }

    getTranslations(url) {
        return this.http.get(this.restConstants.rootUrl + url)
            .pipe(
                catchError(url)
            );
    }
}
