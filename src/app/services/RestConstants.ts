import { Injectable } from '@angular/core';

@Injectable()
export class RestConstants {

	public rootUrl:string = 'https://www.google.ro'; // root for API calls
	public login:string = '/login';
}