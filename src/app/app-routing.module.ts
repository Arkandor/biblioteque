import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import { HomeView } from './views/home/home';
import { BookView } from './views/book/book';

import { ModalComponent } from './components/modal/modal.component';
import { LoadingComponent } from './components/loading/loading.component';

const routes: Routes = [
	{ path: 'home', component: HomeView },
	{ path: 'book', component: BookView },
    { path: '',   redirectTo: '/home', pathMatch: 'full' },
    { path: '**', redirectTo: '/home'}
];

const engineLocation = "assets/";

@NgModule({
	declarations: [
		HomeView,
		BookView,
		ModalComponent,
		LoadingComponent
	],
	imports: [
		CommonModule,
		BrowserModule,
        FormsModule,
        ReactiveFormsModule,
		RouterModule.forRoot(routes),
	],
	exports: [
		RouterModule,
		LoadingComponent
	],
	providers: []
})
export class AppRoutingModule { }
